﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Final_Project_Olga_Neyezhsal_ver2._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
    <div class="col-md-4">
            <h2>Build a .NET development team in Canada, in just 3 days!</h2>
            <p>
                Clarisoft Technologies offers you the opportunity to hire dotNET developers according 
                to your needs and project’s requirements. We are a leading outsourcing development company, 
                with its development team located in Toronto, Canada. We have a transparent rate & cost structure
                and with us, you have the timeline guaranteed! You can hire ASP.Net developers for building custom
                enterprise solutions and grow your business, without investing in the infrastructure costs.
            </p>
            <p>
                <a class="btn-default" href="https://www.clarisoft.com/outsource-net-development-services/?gclid=CjwKCAiA0ajgBRA4EiwA9gFOR4YgqoJjVG6wWAEOBaSkn_qsVs0gV2BbF78agJsXAHws4NhD7oVAZRoCMPMQAvD_BwE">Learn more &raquo;</a>
            </p>
        </div>
        <div class="col-md-4">
            
            <p>
                ASP.Net is widely used for developing robust and scalable websites, software and web applications. 
                Clarisoft provides full-cycle dotNET development solutions that deliver functionality and generate optimal user-experience. 
                Our .NET developers have drawn a solid line of quality work in ASP.NET application development and .NET core framework across various industries.
            </p>
            
        </div>
        <div class="col-md-4">
            
            <p>
                Our developers are available to start working on your project right away!
                So, in you can have a working team of dotNET in three days at your service! We offer solutions that enable 
                you to operate in a resourceful manner and assist you to achieve your organizational goal proficiently. 
                For our new customers we offer 30% off rates the first three months..
            </p>
          
        </div>
    </div>    

       
    
        
</asp:Content>
