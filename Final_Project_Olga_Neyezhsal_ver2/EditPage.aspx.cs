﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace Final_Project_Olga_Neyezhsal_ver2
{
    public partial class EditPage : System.Web.UI.Page
    {

        public int pageid
        {
            get { return Convert.ToInt32(Request.QueryString["pageid"]); }
        }

        protected void Edit_Page(object sender, EventArgs e)
        {
            //change this
            string pagetitle = page_title.Text;
            string pagecontent = page_content.Text;

            string editquery = "Update pages set pagetitle='" + pagetitle + "'," +
                " pagecontent='" + pagecontent +"'" +
                "where pageid=" + pageid;
                

            edit_page.UpdateCommand = editquery;
            edit_page.Update();


        }

        protected DataRowView getPageInfo(int id)
        {
            string query = "select * from pages where pageid=" + pageid.ToString();
            page_select.SelectCommand = query;
            
            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

            
            if (pageview.ToTable().Rows.Count < 1)
            {
                return null;
            }
            DataRowView pagerowview = pageview[0]; 
            return pagerowview;

        }
    }
}