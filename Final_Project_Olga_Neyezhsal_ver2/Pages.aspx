﻿<%@ Page Title="Pages" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pages.aspx.cs" Inherits="Final_Project_Olga_Neyezhsal_ver2.Pages" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
 <h2><%: Title %></h2>
    
    <div id="control">
        <div id="inputrow">
        <a href="NewPage.aspx">New Page</a>
    </div>
    <div id="search">
    <asp:TextBox runat="server" ID="page_key"></asp:TextBox>
    <asp:Button runat="server" Text="Search" OnClick="Search_Pages"/>
    </div>
    </div>
    <div id="query_display" runat="server"></div>
    
    

</asp:Content>
    
 <asp:Content ID="Content2" ContentPlaceHolderID="fullscreen" runat="server">
<asp:SqlDataSource runat="server"
        id="pages_select"
        ConnectionString="<%$ ConnectionStrings:fproject %>">
    </asp:SqlDataSource>
    <asp:DataGrid id="pages_list"
        runat="server" >
    </asp:DataGrid>
</asp:Content>


