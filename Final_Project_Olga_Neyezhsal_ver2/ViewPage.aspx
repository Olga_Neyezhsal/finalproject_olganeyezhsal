﻿<%@ Page Title="ViewPage" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViewPage.aspx.cs" Inherits="Final_Project_Olga_Neyezhsal_ver2.ViewPage" %>
<asp:Content ContentPlaceHolderID="MainContent" runat="server">
    
   <asp:SqlDataSource runat="server"
        id="page_select"
        ConnectionString="<%$ ConnectionStrings:fproject %>">
    </asp:SqlDataSource>

     <div class="title"><h1 runat="server" id="ptitle"></h1></div>
 
   

    
    <asp:SqlDataSource runat="server" id="del_page" ConnectionString="<%$ ConnectionStrings:fproject%>">
    </asp:SqlDataSource>
    
     <div id="del_btn">
        <asp:Button runat="server" id="del_page_btn" Text="Delete" OnClick="FetchID" /></div>

   <div id="edit_btn"><a href="EditPage.aspx?pageid=<%Response.Write(this.pageid);%>">Edit</a></div> 
   
    
 
      <div id="content_div"><p runat="server" id="pcontent"></p></div>
       
</asp:Content>

