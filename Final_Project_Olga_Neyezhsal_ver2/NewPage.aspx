﻿<%@ Page Title="NewPage" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewPage.aspx.cs" Inherits="Final_Project_Olga_Neyezhsal_ver2.NewPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <asp:SqlDataSource runat="server" id="create_page"
        ConnectionString="<%$ ConnectionStrings:fproject %>">
    </asp:SqlDataSource>

    <h3>New Page</h3>

    
    <div class="inputrow">
        <label>Page Title:</label>
        <asp:textbox id="page_title" runat="server"></asp:textbox>

        <asp:RequiredFieldValidator runat="server" ControlToValidate="page_title" ForeColor="Red"
            ErrorMessage="Enter a Page Title">
        </asp:RequiredFieldValidator>
    </div>

    <div class="inputrow">
        <label>Page Content:</label>
        <asp:textbox id="page_content" runat="server">
        </asp:textbox>

        <asp:RequiredFieldValidator runat="server" ControlToValidate="page_content" ForeColor="Red" 
            ErrorMessage="Enter a Page Content">
        </asp:RequiredFieldValidator>
    </div>

  

    <asp:Button Text="Add Page" runat="server" OnClick="AddPage"/>
     
    <div id="query_one" runat="server"></div>

</asp:Content>

