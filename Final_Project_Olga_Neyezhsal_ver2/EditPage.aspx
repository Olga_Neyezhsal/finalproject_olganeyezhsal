﻿<%@ Page Title="Edit Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="Final_Project_Olga_Neyezhsal_ver2.EditPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

     <asp:SqlDataSource runat="server" id="SqlDataSource1"
        ConnectionString="<%$ ConnectionStrings:fproject %>">
    </asp:SqlDataSource>

    <asp:SqlDataSource runat="server" id="edit_page"
        ConnectionString="<%$ ConnectionStrings:fproject %>">
    </asp:SqlDataSource>

    <asp:SqlDataSource runat="server" id="page_select"
        ConnectionString="<%$ ConnectionStrings:fproject %>">
    </asp:SqlDataSource>

    <h3 runat="server" id="page_name">Update </h3>

    <div class="inputrow">
        <label>Page Title:</label>
  
      <asp:TextBox ID="page_title" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator runat="server"
            ControlToValidate="page_title"
            ErrorMessage="Enter a Page Title">
        </asp:RequiredFieldValidator>
    </div>
    
    <div class="inputrow">
        <label>Page Content</label>
        <asp:Textbox runat="server" id="page_content"  ></asp:Textbox>
        <asp:RequiredFieldValidator ID="page_content" 
            controlToValidate="page_content"></asp:RequiredFieldValidator>
    </div>
    
    <div>
    <asp:Button Text="Edit Page" runat="server" OnClick="Edit_Page"/>
    </div>
   

</asp:Content>
