﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Final_Project_Olga_Neyezhsal_ver2
{
    public partial class Pages : System.Web.UI.Page
    {
        private string sql = "SELECT pageid, pagetitle, pagecontent from Pages ";

        protected void Page_Load(object sender, EventArgs e)
        {
            pages_select.SelectCommand = sql;
           

            pages_list.DataSource = Pages_Manual_Bind(pages_select);


            pages_list.DataBind();
        }



        protected DataView Pages_Manual_Bind(SqlDataSource src)
        {

            DataTable mytbl;


            DataView myview;


            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;


            foreach (DataRow row in mytbl.Rows)
            {

                row["pagetitle"] =
                   "<a href=\"ViewPage.aspx?pageid="
                   + row["pageid"]
                   + "\">"
                   + row["pagetitle"]
                   + "</a>";

            }
            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;

            return myview;
             }
        protected void Search_Pages(object sender, EventArgs e)
        {
            string newsql = sql + " where (1=1)";
            string key = page_key.Text;

            if (key != "")
            {
                newsql +=
                    " AND PAGETITLE LIKE '%" + key + "%'"; 
            }
            query_display.InnerHtml = newsql;
            pages_select.SelectCommand = newsql;
     
            pages_list.DataSource = Pages_Manual_Bind(pages_select);

            pages_list.DataBind();
        }

    }
}
    


