﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Final_Project_Olga_Neyezhsal_ver2
{
    public partial class ViewPage : System.Web.UI.Page
    {
        
        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }

        private string page_basequery =
            "SELECT * from Pages";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (pageid == "" || pageid == null)
                ptitle.InnerHtml = "No page found.";
            else
            {
                page_basequery += " WHERE PAGEID = " + pageid;
                page_select.SelectCommand = page_basequery;


                DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);

                foreach (DataRowView row in pageview)
                {
                    string pagetitle = row["pagetitle"].ToString();
                    string pagecontent = row["pagecontent"].ToString();
                    ptitle.InnerHtml = pagetitle;
                    pcontent.InnerHtml = pagecontent;
                }

            }
        }
        protected void FetchID(object sender, EventArgs e)
        {

            DelPage(pageid);
        }

        protected void DelPage(string pageid)
        {
            string basequery = "DELETE FROM PAGES WHERE PAGEID=" + pageid;
            del_page.DeleteCommand = basequery;
            del_page.Delete();
            Response.Redirect("Pages.aspx");


        }
    }
}